import { Component } from '@angular/core';
import { Operation } from '../enums/operation';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  result: number

  number1: number
  number2: number

  operation: Operation

  Operation = Operation


  constructor() {
    this.result = 0
  }


  setValue(digit: number): void {
    let concat: string = `${this.result}${digit}`;
    if(concat[0] == '0') {
      concat = concat.substr(1);
    }

    this.result = Number(concat);
  }

  setOperation(operation: Operation): void {
    this.number1 = this.result;

    this.result = 0;

    this.operation = operation;
  }

  total(): void {

    this.number2 = this.result;

    switch(this.operation) {
      case Operation.SUMA:
        this.result = this.number1 + this.number2;
      break;
      case Operation.RESTA:
        this.result = this.number1 - this.number2;
      break;
      case Operation.MULTIPLICACION:
        this.result = this.number1 * this.number2;
      break;
      case Operation.DIVISION:
        if (this.number1 != 0 || this.number2 != 0) {
          this.result = this.number1 / this.number2;
        }
        
      break;
    }
  }

  reset() : void {
    this.number1 = 0;
    this.number2 = 0;
    this.result = 0;
  }

}
